function divElementEnostavniTekst(sporocilo) {
  var sprememba=sporocilo.replace( ";)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/wink.png'>");
  sprememba=sprememba.replace( ':)', '<img src="https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/smiley.png">');
  sprememba=sprememba.replace( "(y)", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/like.png'>");
  sprememba=sprememba.replace( ":*", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/kiss.png'>");
  sprememba=sprememba.replace( ":(", "<img src='https://dl.dropboxusercontent.com/u/2855959/moodle/common/emoticons/sad.png'>");
  return $('<div style="font-weight: bold"></div>').html(sprememba);
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

//Tabela spornih besed
var sporneBesede = [];

//Parsanje swearWords.txt
$.get ('swearWords.txt', function (file) {
  var tabelaVrstic = file.split("\n");
  for (var i = 0; i < tabelaVrstic.length; i++) {
    sporneBesede.push(tabelaVrstic[i]);
  }
});

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    //Popravljanje kanala
    var praviKanal = $('#kanal').text().split(" ");
    klepetApp.posljiSporocilo(praviKanal[2], sporocilo);
    
    //Zamenjava sporne besede v kolikor je to potrebno
    var uporabnikovoSporocilo = sporocilo.split(" ");
    for (var i = 0; i < uporabnikovoSporocilo.length; i++) {
      for (var j = 0; j < sporneBesede.length; j++) {
        if (uporabnikovoSporocilo[i] === sporneBesede[j]) {
          var tempSporocilo = "";
          for (var k = 0; k < uporabnikovoSporocilo[i].length; k++) {
            tempSporocilo = tempSporocilo + "*";
          }
          sporocilo=sporocilo.replace(uporabnikovoSporocilo[i], tempSporocilo);
        }
      }
    }
    

    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var delujocKanal, delujocVzdevek;

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      
      delujocVzdevek = rezultat.vzdevek;
      //Nov izpis
      $('#kanal').text(delujocVzdevek + " @ " + delujocKanal);
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    delujocKanal = rezultat.kanal;
    //Nov izpis
    $('#kanal').text(delujocVzdevek + " @ " + delujocKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = $('<div style="font-weight: bold"></div>').text(sporocilo.besedilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo.besedilo));
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    //uporabniki
    socket.emit('tabelaUporabniki');
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });

  socket.on('tabelaUporabniki', function(tabelaUporabniki) {
   $('#seznam-uporabnikov').empty();
   for (var i = 0; i < tabelaUporabniki.length; i++) {
     $('#seznam-uporabnikov').append(divElementEnostavniTekst(tabelaUporabniki[i]));
   }
  });
});
